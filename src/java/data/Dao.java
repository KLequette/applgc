package data;
import entites.Client;
import entites.Facture;
import entites.Region;
import java.util.List;

public class Dao {
    
    public static Region           getRegionDeCode(String pCodeReg)  {
    
        Region reg=null;
        
        for (Region r : DonneesMem.getToutesLesRegions()){
        
            if (r.getCodeRegion().equals(pCodeReg)){reg=r;break;}
            
        }
        
        return reg;
    
    
    }
    public static List<Region>     getToutesLesRegions()             {
        return DonneesMem.getToutesLesRegions();
    }
        
    public  static Client          getClientDeNumero(Long pNumCli )  {
         
        Client  cli=null;
     
        for(Client  c: DonneesMem.getTousLesClients()){
         
         if( c.getNumCli().equals(pNumCli) ){ cli=c;break;}
        }
        return cli;
    }
    public static List<Client>     getTousLesClients()               {
         
        
        return DonneesMem.getTousLesClients();
    
    }
   
    public static Facture          getFactureDeNumero(Long pNumFact) {
    
        Facture fact=null;
        
        
        for (Facture f : DonneesMem.getToutesLesFactures()){
        
           if(f.getNumFact().equals(pNumFact)){fact=f;break;} 
        }
        
        return fact;

    }
    public static List<Facture>    getToutesLesFactures()            { 
        
        return DonneesMem.getToutesLesFactures(); }   
}
