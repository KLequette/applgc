
package web.services;

import data.Dao;
import dto.DtoRegion;
import entites.Region;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Stateless
@Path("region")
public class WebServGC {
    
    @GET
    @Path("infos/{codereg}")
    @Produces({
        "application/xml",
        "application/json"})
    public DtoRegion 
    getLaRegion(@PathParam("codereg") String codereg) {
       
        Region r= Dao.getRegionDeCode(codereg);
        
       DtoRegion    dtoR= new DtoRegion();
        
       // remplir dtR
       
       return dtoR;
    }   
}
