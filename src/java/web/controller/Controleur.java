
package web.controller;

import data.Dao;
import entites.Region;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Controleur {
    
    String codeRegion;
    
    Region reg;

    
    public void recherche(){
    reg=Dao.getRegionDeCode(codeRegion);
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="getset">
    public String getCodeRegion() {
        return codeRegion;
    }
    
    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }
    
    public Region getReg() {
        return reg;
    }
    
    public void setReg(Region reg) {
        this.reg = reg;
    }
    
    
    //</editor-fold>
    
    
    
    
}
